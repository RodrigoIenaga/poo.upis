package upis;

public class HorarioSo1 {

//usar como teste, o numero 7265 -> 2h, 1min, 5s ou 2:01:05	
	
	private int segundo;

//atribuidores	

	public HorarioSo1 () {
		setSegundo((byte)0);
	}
	
	public HorarioSo1 (byte hora, byte minuto, byte segundo) {
		setHora(hora);
		setMin(minuto);
		setSegundo(segundo);
	}
//set	
	
	public void setHora(byte hora) {
		if(hora >=0 && hora <=23) {
			int h = segundo/3600;			
			int m = (segundo - h*3600)/60;
			int s = (segundo - h*3600) - m*60;
			segundo = hora*3600 + m*60 + s;		
		}
	}	
	public void setMin(byte minuto) {
		if(minuto >=0 && minuto <=59) {
			int h = segundo/3600;
			int m = (segundo - h*3600)/60;
			int s = segundo - h*3600 - m*60;
			segundo = h*3600 + s + minuto*60;
		}
	}
	public void setSegundo(byte seg) {
		if(seg>=0 && seg<=59) {
			int h = segundo/3600;
			int m = (segundo - h*3600)/60;
			segundo = h*3600 + seg + m*60;
		}
	}
	
//get
	
	public int getHora() {
		int var;
		var = (segundo/3600);
		return var;
	}
	public int getMinuto() {
		int minuto;
		int hora;
		hora = (segundo/3600);
		minuto = (segundo - hora*3600)/60;
		return minuto;
	}
	public int getSeg() {
		int hora;
		int min;
		int seg;
		hora = (segundo/3600);
		min = (segundo - hora*3600)/60;
		seg = (segundo - hora*360 - min*60);
		return seg;
	}

//string

	public String toString() {
		return getHora() + ":" + getMinuto() + ":" + getSeg();
	}
	
//iteracao
	
	public void incrementaSegundo() {
		if (segundo < 86400) {
			segundo = segundo + 1;
		}
	}
	public void incrementaMinuto() {
		if (segundo/60 < 1440) {
			segundo = segundo + 60;
		}
	}
	public void incrementaHora() {
		if (segundo/3600 < 24) {
			segundo = segundo + 3600;
		}
	}
	
	public boolean equals(Object obj) {
		
		if(this == obj)
			return true;
		
		if(obj == null || obj.getClass()!= this.getClass())
			return false;
		
		HorarioSo1 h = (HorarioSo1) obj;
		
		return this.getHora() == h.getHora() && 
				this.getMinuto() == h.getMinuto() && 
				this.getSeg() == h.getSeg();
	}	
	
	public boolean lt(Object obj) {
		
		HorarioSo1 hr = (HorarioSo1) obj;
		
		if (this.segundo < hr.segundo) {
			
			return true;
		}
		else
			return false;
		}
		
		public boolean le(Object obj) {
			
			return this.equals(obj) || this.lt(obj);
		}
		
		public boolean ge(Object obj) {
			
			return !(this.lt(obj));
		
		}
		
		public boolean gt(Object obj) {
			
			return !(this.le(obj));
		}
		
		public int compare(HorarioSo1 o1, HorarioSo1 o2) {
			if(o1.lt(o2)) return -1;
			if(o1.equals(o2)) return 0;
			return 1;
		}
					
	}
	