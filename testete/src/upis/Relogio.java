package upis;

public class Relogio {

	private Horario hms;
	private Data dma;
	
	public Relogio(Horario hms, Data dma) {
		this.hms = new Horario(hms);
		this.dma = dma;
	}
	
	public void tictac() {
		
		hms.incrementaSegundo();

		if(hms.ehPrimeiroHorario()) {
			dma.incrementaDia();
		}		
	}
	
	@Override
	public String toString() {
		return dma + " " + hms;
	}
	
	public boolean equals(Object obj) {
		
		if(this == obj)
			return true;
		
		if(obj == null || obj.getClass()!= this.getClass())
			return false;
		
		Relogio r = (Relogio) obj;
		
		return this.hms.getHora() == r.hms.getHora() &&
				this.hms.getMinuto() == r.hms.getMinuto() && 
				this.hms.getSegundo() == r.hms.getSegundo() && 
				this.dma.getAno() == r.dma.getAno() &&
				this.dma.getMes() == r.dma.getMes() && 
				this.dma.getDia() == r.dma.getDia();
	}	
	
	public boolean lt(Object obj) {
		
		Relogio r = (Relogio) obj;
	
		if(
			(this.dma.getAno() < r.dma.getAno() )||
			(this.dma.getAno() == r.dma.getAno() && this.dma.getMes() < r.dma.getMes())||
			(this.dma.getAno() == r.dma.getAno() && this.dma.getMes() == r.dma.getMes() && this.dma.getDia() < r.dma.getDia())||
			(this.dma.getAno() == r.dma.getAno() && this.dma.getMes() == r.dma.getMes() && this.dma.getDia() == r.dma.getDia() && this.hms.getHora() < r.hms.getHora())||
			(this.dma.getAno() == r.dma.getAno() && this.dma.getMes() == r.dma.getMes() && this.dma.getDia() == r.dma.getDia() && this.hms.getHora() == r.hms.getHora() && this.hms.getMinuto() < r.hms.getMinuto())||
			(this.dma.getAno() == r.dma.getAno() && this.dma.getMes() == r.dma.getMes() && this.dma.getDia() == r.dma.getDia() && this.hms.getHora() == r.hms.getHora() && this.hms.getMinuto() == r.hms.getMinuto() && this.hms.getSegundo() < r.hms.getSegundo())
		)
			return true;
		else
			return false;
	}
		
	public boolean le(Object obj) {
		
		return this.equals(obj) || this.lt(obj);
	}
	
	public boolean ge(Object obj) {
		
		return !(this.lt(obj));
	}
	
	public boolean gt(Object obj) {
		
		return !(this.le(obj));
	}
	
	public int compare(Relogio r1, Relogio r2) {
		if(r1.lt(r2)) return -1;
		if(r1.equals(r2)) return 0;
		return 1;
	}
}

