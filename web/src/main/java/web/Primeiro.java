package web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/primeiro")
public class Primeiro extends HttpServlet {
	
	private int contador;
	
	@Override
	public void init() throws ServletException {
		System.out.println("Servlet Iniciado.");
		System.out.println("Tentativas de login: " + contador);
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		PrintWriter out = resp.getWriter();
		
		String login = req.getParameter("usuario");
		String senha = req.getParameter("senha");
		
		contador = contador + 1; 
		
		out.println("<html>");
		out.println("<head>");
		out.println("</head>");
		out.println("<body>");
		out.println("<h1>Log in com contador</h1>");
		if(login.equals("admin") && senha.equals("1234")) {
			out.println("<h3>Seja bem vindo, " + login + "</h3>");
			out.println("<h1>Tentativa de n�mero: " + contador + "</h3>");
		}
		else {
			out.println("usuario ou senha incorreto");
			out.println("<h1>Tentativa de n�mero: " + contador + "</h3>");
		}
		out.println("</body>");
		out.println("</html>");		
	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		PrintWriter out = resp.getWriter();
		
		String login = req.getParameter("usuario");
		String senha = req.getParameter("senha");
		
		contador = contador + 1; 
		
		out.println("<html>");
		out.println("<head>");
		out.println("</head>");
		out.println("<body>");
		out.println("<h1>Log in com contador</h1>");
		if(login.equals("admin") && senha.equals("1234")) {
			out.println("<h3>Seja bem vindo, " + login + "</h3>");
			out.println("<h1>Tentativa de n�mero: " + contador + "</h3>");
		}
		else {
			out.println("usuario ou senha incorreto");
			out.println("<h1>Tentativa de n�mero: " + contador + "</h3>");
		}
		out.println("</body>");
		out.println("</html>");

	}
	
	@Override
	public void destroy() {
		System.out.println("Servlet destruido.");
		System.out.println("Contador final: " + contador);
	}

	
}