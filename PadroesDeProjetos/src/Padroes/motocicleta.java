package Padroes;

public class motocicleta implements IVeiculoAutomotor{
	
	private int rodas;
	private String Objetivo;
	
	public motocicleta() {
		rodas = 2;
		Objetivo = "Transporte de passageiro, ate 2 pessoas";
	}
	
	public int getRoda() {
		return rodas;
	}
	
	public String getObj(){
		return Objetivo;
	}
	
	public int qqtrodas() {
		int r = getRoda();
		return r;
	}
	
	public String objetivo() {
		String s = getObj();
		return s;
	}
}

