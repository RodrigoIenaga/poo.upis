package Padroes.singleton;

import java.util.Scanner;

public class MockCalculator {

	private static MockCalculator instance[];
	
	private int i;
	private static int max;
	
	private MockCalculator() {
	}
	
	public void setMax(int a) {
		Scanner sc = new Scanner(System.in);
		System.out.println("digite o numero maximo de instancias: ");
		max = sc.nextInt();
	}
	public MockCalculator getInstance() {
		
		if(instance == null) {
			
			instance = new MockCalculator[max];
			criaInstancia();
		}
		
		i++;
		if(i==max) {
			i=0;
			
		}
		return instance[i];
	}
	
	private static void criaInstancia() {
		for(int j = 0; j< max; j++) {
			instance[j] = new MockCalculator();
		}
	}
	
	public int soma(int a, int b) {
		return a + b;
	}
	
	public int subtracao(int a, int b) {
		return a - b;
	}
	
	public int multiplicacao(int a, int b) {
		return a * b;	
	}
	
	public int divisao(int a, int b) {
		return a/b;	
	}
}
