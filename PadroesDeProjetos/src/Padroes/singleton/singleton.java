package Padroes.singleton;

public class singleton {
	
	private static singleton instance;
	
	private singleton() {
		
	}
	
	public singleton getInstance() {
		
		if(instance == null) {
			instance = new singleton();
		}

		return instance;

	}
}
